var https = require('https');
var fs = require('fs');
var body='';
exports.test = function testRequest(callback){
const options = {
  hostname: 'encrypted.google.com',
  port: 443,
  path: '/',
  method: 'GET'
};
const req = https.request('https://jsonplaceholder.typicode.com/todos/1', (res) => {
  console.log('statusCode:', res.statusCode);
 // console.log('headers:', res.headers);

  res.on('data', (d) => {
   // process.stdout.write(d);
   body+= d;
   
  });
  res.on('end', function(){
    callback(body);
  } );
});

req.on('error', (e) => {
  console.error(e);
  callback(e);
});
req.end();
};

